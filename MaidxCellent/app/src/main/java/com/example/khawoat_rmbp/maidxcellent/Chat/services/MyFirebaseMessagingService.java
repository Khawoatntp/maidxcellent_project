package com.example.khawoat_rmbp.maidxcellent.Chat.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.example.khawoat_rmbp.maidxcellent.Chat.util.BadgeUtils;
import com.example.khawoat_rmbp.maidxcellent.MainAfterLogin;
import com.example.khawoat_rmbp.maidxcellent.R;
import com.example.khawoat_rmbp.maidxcellent.SplashScreen.splashScreen;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by KHAWOAT-rMBP on 7/12/2017 AD.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    private static final String TAG = "Message";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

//        Log.d(TAG, "From: " + remoteMessage.getFrom());

        sharedPreferences = getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);

        String name = remoteMessage.getData().get("name");
        String message = remoteMessage.getData().get("message");
        String tokan = remoteMessage.getData().get("tokan");
        String image = remoteMessage.getData().get("image");
        BadgeUtils.setBadge(this, 1);
        sendNotification(name,message,tokan,image);
    }

    private void sendNotification(String name, String message, String tokan, String image) {
        Intent intent = new Intent(this, MainAfterLogin.class);
        // Configs.TOKEN_USER = tokan;
        //  Configs.chatWith = name;
        //  Configs.ImageUserChat = image;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(splashScreen.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.testlogo)
                .setLargeIcon(getBitmapFromURL(Config.ImageUser))
                .setContentTitle(name)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[] {500, 1000, 500})
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentText(message);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

//    private void restartApp(){
//        editor = sharedPreferences.edit();
//        editor.putBoolean("isKick", true);
//        editor.putBoolean("isLogout", true);
//        editor.apply();
//        Handler h = new Handler(Looper.getMainLooper());
//        h.post(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//            }
//        });
//    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


}
