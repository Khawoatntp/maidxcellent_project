package com.example.khawoat_rmbp.maidxcellent.Chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.khawoat_rmbp.maidxcellent.BaseActivity;
import com.example.khawoat_rmbp.maidxcellent.Chat.services.SinchService;
import com.example.khawoat_rmbp.maidxcellent.Chat.util.HttpRequest;
import com.example.khawoat_rmbp.maidxcellent.R;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.example.khawoat_rmbp.maidxcellent.videosCall.CallScreenActivity;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sinch.android.rtc.calling.Call;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Chat extends BaseActivity implements View.OnClickListener {

    LinearLayout layout, lvChat;
    ImageView sendButton;
    EditText messageArea;
    private String strDate ="";
    private TextView tvNameChat;
    private Button bntCall;
    ScrollView scrollView;
    Firebase reference1, reference2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Firebase.setAndroidContext(this);

        bindView();
        setView();
    }

    private void bindView(){
        tvNameChat = (TextView) findViewById(R.id.tv_namechat);
        layout = (LinearLayout)findViewById(R.id.layout1);
        sendButton = (ImageView)findViewById(R.id.sendButton);
        messageArea = (EditText)findViewById(R.id.messageArea);
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        bntCall = (Button)findViewById(R.id.bnt_call);
    }

    private void setView(){
        tvNameChat.setText(Config.chatWith);
        sendButton.setOnClickListener(this);
        bntCall.setOnClickListener(this);

        reference1 = new Firebase("https://chat-12431.firebaseio.com/messages/" + Config.username + "_" + Config.chatWith);
        reference2 = new Firebase("https://chat-12431.firebaseio.com/messages/" + Config.chatWith + "_" + Config.username);

        reference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String time = map.get("time").toString();
                String userName = map.get("user").toString();

                if(userName.equals(Config.username)){
                    addMessageBox(message,time, 1);
                }
                else{
                    addMessageBox(message,time, 2);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {

        if(v==sendButton){
            chackMessage();
        }else if(v==bntCall){
            callButtonClicked();
        }

    }

    private void chackMessage(){
        String messageText = messageArea.getText().toString();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        strDate = sdf.format(c.getTime());
        if(!messageText.equals("")){
            Map<String, String> map = new HashMap<String, String>();
            map.put("message", messageText);
            map.put("time", strDate);
            map.put("user", Config.username);
            reference1.push().setValue(map);
            reference2.push().setValue(map);
            messageArea.setText("");
            messageArea.clearAnimation();
            noti(messageText);
        }
    }

    private void addMessageBox(String message, String time, int type){

        if(type == 1) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View viewUser = layoutInflater.inflate(R.layout.view_user, null);
            TextView tvMessage = (TextView) viewUser.findViewById(R.id.tv_message);
            TextView tvTime = (TextView) viewUser.findViewById(R.id.tv_time);
            tvMessage.setText(message);
            tvTime.setText(time+".");
            layout.addView(viewUser);
        }
        else{
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View ViewUsetChat = layoutInflater.inflate(R.layout.view_userchat, null);
            final ImageView imvUser = (ImageView) ViewUsetChat.findViewById(R.id.imv_user);
            if(imvUser.getBackground()==null){
                Glide.with(Chat.this).load(Config.ImageUserChat).asBitmap().error(getResources().getDrawable(R.drawable.profile)).centerCrop().into(new BitmapImageViewTarget(imvUser) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(Chat.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imvUser.setImageDrawable(circularBitmapDrawable);
                    }
                });
            }else {

            }
            TextView tvMessage = (TextView) ViewUsetChat.findViewById(R.id.tv_message);
            TextView tvTime = (TextView) ViewUsetChat.findViewById(R.id.tv_time);
            tvMessage.setText(message);
            tvTime.setText(time+".");
            layout.addView(ViewUsetChat);
        }
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    //to place the call to the entered name
    private void callButtonClicked() {
        String userName = Config.chatWith.toString();
        if (userName.isEmpty()) {
            Toast.makeText(this, "Please enter a user to call", Toast.LENGTH_LONG).show();
            return;
        }

        Call call = getSinchServiceInterface().callUserVideo(userName);
        String callId = call.getCallId();

        Intent callScreen = new Intent(this, CallScreenActivity.class);
        callScreen.putExtra(SinchService.CALL_ID, callId);
        startActivity(callScreen);
    }

    private void noti(String messageText){
        Ion.with(this)
                .load(HttpRequest.NOTI)
                .setMultipartParameter("token",Config.TOKEN_USER.toString())
                .setMultipartParameter("name", Config.username)
                .setMultipartParameter("message", messageText)
                .setMultipartParameter("image", Config.ImageUser)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                    }
                });
    }

}
