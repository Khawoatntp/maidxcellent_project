package com.example.khawoat_rmbp.maidxcellent.Chat.services;

import android.util.Log;

import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by KHAWOAT-rMBP on 7/7/2017 AD.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token){
        Config.FCM_TOKEN = token;
    }

}
