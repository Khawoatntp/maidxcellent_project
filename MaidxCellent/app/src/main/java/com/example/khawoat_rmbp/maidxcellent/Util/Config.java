package com.example.khawoat_rmbp.maidxcellent.Util;

/**
 * Created by KHAWOAT-rMBP on 6/21/2017 AD.
 */

public class Config {
    public static String PROFILE_USER_ID = "USER_ID";
    public static String PROFILE_FIRST_NAME = "PROFILE_FIRST_NAME";
    public static final String PROFILE_LAST_NAME = "PROFILE_LAST_NAME";
    public static String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";
    public static String PROFILE_EMAIL = "PROFILE_EMAIL";

    public static final String PREFS_NAME = "chat_prefs";
    public static final String MEMO_TYPE = "chat_type";
    public static final String LOGIN = "login";
    public static final String ISCAS = "isCas";
    public static String FCM_TOKEN;
    public static String USERNAME;
    public static String DEVICE_TOKEN;
    public static String ImageUser="";
    public static String username = "";
    public static String password = "";
    public static String chatWith = "";
    public static String TOKEN_USER = "";
    public static String ImageUserChat = "";
}
