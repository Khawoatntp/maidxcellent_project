package com.example.khawoat_rmbp.maidxcellent;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.khawoat_rmbp.maidxcellent.SocialLogin.LoginActivity;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.firebase.client.Firebase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class Register extends BaseActivity implements View.OnClickListener {

    private EditText username, password, etEmail, etTel;
    private Button registerButton, bntBack;
    private String user, pass, email, tel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Firebase.setAndroidContext(this);

        bindView();
        setView();
    }

    private void bindView(){
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        etEmail = (EditText)findViewById(R.id.email);
        etTel = (EditText)findViewById(R.id.tel);
        registerButton = (Button)findViewById(R.id.registerButton);
        bntBack= (Button)findViewById(R.id.bnt_back);
    }

    private void setView(){
        registerButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==bntBack){
            onBackPressed();
        }else if(v==registerButton){
            chackPutData();
        }
    }

    private void chackPutData(){
        user = username.getText().toString();
        pass = password.getText().toString();
        email = etEmail.getText().toString();
        tel = etTel.getText().toString();

        if(user.equals("")){
            username.setError("can't be blank");
        }
        else if(pass.equals("")){
            username.setError("can't be blank");
        }
        else if(!user.matches("[A-Za-z0-9]+")){
            username.setError("only alphabet or number allowed");
        }
        else if(user.length()<5){
            username.setError("at least 5 characters long");
        }
        else if(pass.length()<5){
            password.setError("at least 5 characters long");
        }
        else if(email.length()<5){
            etEmail.setError("at characters long");
        }
        else if(tel.length()<10){
            etTel.setError("at characters long");
        }
        else {
            setRegister();
        }
    }

    private void setRegister(){
        final ProgressDialog pd = new ProgressDialog(Register.this);
        pd.setMessage("Loading...");
        pd.show();

        String url = "https://maidxcellent-64af3.firebaseio.com/user.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                Firebase reference = new Firebase("https://maidxcellent-64af3.firebaseio.com/user");

                if(s.equals("null")) {
                    HashMap<String,Object> taskMap = new HashMap<String,Object>();
                    taskMap.put("image",  "null");
                    taskMap.put("tokan",  Config.FCM_TOKEN);
                    taskMap.put("password",  pass);
                    taskMap.put("username", user);
                    taskMap.put("email",  email);
                   // taskMap.put("tel",  tel);
                    reference.child(user).setValue(taskMap);
                    Toast.makeText(Register.this, "registration successful", Toast.LENGTH_LONG).show();
                }
                else {
                    try {
                        JSONObject obj = new JSONObject(s);

                        if (!obj.has(user)) {
                            HashMap<String,Object> taskMap = new HashMap<String,Object>();
                            taskMap.put("image",  "null");
                            taskMap.put("tokan",  Config.FCM_TOKEN);
                            taskMap.put("password",  pass);
                            taskMap.put("username", user);
                            taskMap.put("email",  email);
                         //   taskMap.put("tel",  tel);
                            reference.child(user).setValue(taskMap);
                            Toast.makeText(Register.this, "registration successful", Toast.LENGTH_LONG).show();
                            openActivityFinish(LoginActivity.class);
                        } else {
                            Toast.makeText(Register.this, "username already exists", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }

        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError );
                pd.dismiss();
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(Register.this);
        rQueue.add(request);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}