package com.example.khawoat_rmbp.maidxcellent.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.khawoat_rmbp.maidxcellent.Chat.model.UserChat;

import java.util.List;

/**
 * Created by KHAWOAT-rMBP on 7/12/2017 AD.
 */

public class CommandUser {

    private String key;

    private List<UserChat> value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<UserChat> getData() {
        return value;
    }

    public void setData(List<UserChat> value) {
        this.value = value;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeList(this.value);
    }

    public static final Parcelable.Creator<CommandUser> CREATOR
            = new Parcelable.Creator<CommandUser>() {
        public CommandUser createFromParcel(Parcel in) {
            return new CommandUser(in);
        }

        public CommandUser[] newArray(int size) {
            return new CommandUser[size];
        }
    };

    private CommandUser(Parcel in) {
        in.readList(this.value, null);
    }

}
