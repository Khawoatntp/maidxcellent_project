package com.example.khawoat_rmbp.maidxcellent.SocialLogin;

/**
 * Created by KHAWOAT-rMBP on 7/2/2017 AD.
 */

public class User {

    public static String name;

    public static String email;

    public static String facebookID;

    public static String gender;

    public static String image;

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getFacebookID() {
        return facebookID;
    }

    public String getGender() {
        return gender;
    }

    public String getImage() {
        return image;
    }
}
