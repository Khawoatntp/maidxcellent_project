package com.example.khawoat_rmbp.maidxcellent.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.khawoat_rmbp.maidxcellent.Chat.model.Login;

import java.util.List;

/**
 * Created by KHAWOAT-rMBP on 7/12/2017 AD.
 */

public class CommandLogin implements Parcelable {

    private List<Login> data;

    public List<Login> getData() {
        return data;
    }

    public void setData(List<Login> data) {
        this.data = data;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeList(this.data);
    }

    public static final Parcelable.Creator<CommandLogin> CREATOR
            = new Parcelable.Creator<CommandLogin>() {
        public CommandLogin createFromParcel(Parcel in) {
            return new CommandLogin(in);
        }

        public CommandLogin[] newArray(int size) {
            return new CommandLogin[size];
        }
    };

    private CommandLogin(Parcel in) {
        in.readList(this.data, null);
    }

}
