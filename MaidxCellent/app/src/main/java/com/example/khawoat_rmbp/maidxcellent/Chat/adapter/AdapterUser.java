package com.example.khawoat_rmbp.maidxcellent.Chat.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.khawoat_rmbp.maidxcellent.Chat.model.UserChat;
import com.example.khawoat_rmbp.maidxcellent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KHAWOAT-rMBP on 7/12/2017 AD.
 */

public class AdapterUser extends BaseAdapter {


    private List<UserChat> userChatList;
    private Context context;

    public AdapterUser(Context context, List<UserChat> userChatList) {
        this.userChatList = new ArrayList<>();
        this.userChatList.addAll(userChatList);
        this.context = context;
    }

    @Override
    public int getCount() {
        return userChatList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final AdapterUser.ViewHolder holder = new AdapterUser.ViewHolder();
        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.itemview_user, parent, false);
        }

        holder.imvProfileUser = (ImageView) convertView.findViewById(R.id.imv_profile_user);

        Glide.with(context).load(userChatList.get(position).getImage()).asBitmap().error(context.getResources().getDrawable(R.drawable.profile)).centerCrop().into(new BitmapImageViewTarget(holder.imvProfileUser) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.imvProfileUser.setImageDrawable(circularBitmapDrawable);
            }
        });

        holder.tvName = (TextView) convertView.findViewById(R.id.tv_namechat);
        holder.tvName.setText(userChatList.get(position).getUsername());

        holder.tvMail = (TextView) convertView.findViewById(R.id.tv_mail);
        holder.tvMail.setText(userChatList.get(position).getEmail());

        return convertView;
    }

    private class ViewHolder {
        private TextView tvName,tvMail;
        private ImageView imvProfileUser;
    }

}
