package com.example.khawoat_rmbp.maidxcellent.Chat.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by KHAWOAT-rMBP on 7/12/2017 AD.
 */

public class UserChat implements Parcelable {

    private String email;

    private String image;

    private String password;

    private String tel;

    private String username;

    private String tokan;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTokan() {
        return tokan;
    }

    public void setTokan(String tokan) {
        this.tokan = tokan;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.email);
        out.writeString(this.image);
        out.writeString(this.password);
        out.writeString(this.username);
        out.writeString(this.tokan);
    }

    public static final Parcelable.Creator<UserChat> CREATOR
            = new Parcelable.Creator<UserChat>() {
        public UserChat createFromParcel(Parcel in) {
            return new UserChat(in);
        }

        public UserChat[] newArray(int size) {
            return new UserChat[size];
        }
    };

    public UserChat(String email, String image, String password, String username, String tokan ) {
        this.email = email;
        this.image = image;
        this.password = password;
        this.username = username;
        this.tokan = tokan;
    }

    public UserChat(Parcel in) {
        this.email = in.readString();
        this.image = in.readString();
        this.password = in.readString();
        this.username = in.readString();
        this.tokan = in.readString();
    }

}
