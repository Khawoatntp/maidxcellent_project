package com.example.khawoat_rmbp.maidxcellent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.khawoat_rmbp.maidxcellent.Fragment.FragmentOne;
import com.example.khawoat_rmbp.maidxcellent.SocialLogin.LoginActivity;
import com.example.khawoat_rmbp.maidxcellent.SocialLogin.PreferenceUtils;
import com.example.khawoat_rmbp.maidxcellent.SocialLogin.User;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
//import com.facebook.AppEventsLogger;
import com.facebook.FacebookActivity;
//import com.facebook.Session;
//import com.facebook.android.Facebook;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

public class MainAfterLogin extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        FragmentOne.OnFragmentInteractionListener {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = PreferenceUtils.getCurrentUser(MainAfterLogin.this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);

//        if(PreferenceUtils.getCurrentUser(MainAfterLogin.this) == null){
//            Intent homeIntent = new Intent(MainAfterLogin.this, LoginActivity.class);
//
//            startActivity(homeIntent);
//
//            finish();
//        }

        setContentView(R.layout.activity_main_after_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        View headerLayout = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView profileView = (ImageView)headerLayout.findViewById(R.id.imageView);
        TextView profileName = (TextView)headerLayout.findViewById(R.id.profile_name);
        TextView profileUserEmail = (TextView)headerLayout.findViewById(R.id.textView);
       String profileUserEMAIL = Config.PROFILE_EMAIL;
//     String profileFirstName = returnValueFromBundles(Config.PROFILE_FIRST_NAME);
//        String profileLastName = returnValueFromBundles(Config.PROFILE_LAST_NAME);
//       String profileImageLink = returnValueFromBundles(Config.PROFILE_IMAGE_URL);
//       profileName.setText(profileFirstName + " " + profileLastName);

        //## Facebook ##
//        profileName.setText(User.name);

        profileName.setText(Config.username);
           profileUserEmail.setText(User.email);
 //       profileUserId.setText("User ID : " + profileUserID);
        Picasso.with(MainAfterLogin.this).load(User.image).into(profileView);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            LoginManager.getInstance().logOut();
            Intent logoutIntent = new Intent(MainAfterLogin.this, FacebookActivity.class); startActivity(logoutIntent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        if (id == R.id.new_Booking) {
            // Handle the camera action


        } else if (id == R.id.my_Booking) {

        } else if (id == R.id.message) {

            fragmentClass = FragmentOne.class;


        } else if (id == R.id.notification) {

        } else if (id == R.id.setting) {

        } else if (id == R.id.logout) {


            // We can logout from facebook by calling following method
             LoginManager.getInstance().logOut();
            PreferenceUtils.clearCurrentUser(MainAfterLogin.this);

            user.setEmail("");
            user.setFacebookID("");
            user.setGender("");
            user.setImage("");
            user.setName("");

            Intent i= new Intent(MainAfterLogin.this,LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private String returnValueFromBundles(String key){
        Bundle inBundle = getIntent().getExtras();
        String returnedValue = inBundle.get(key).toString();
        return returnedValue;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
