package com.example.khawoat_rmbp.maidxcellent.SplashScreen;

import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.khawoat_rmbp.maidxcellent.BaseActivity;
import com.example.khawoat_rmbp.maidxcellent.Chat.services.SinchService;
import com.example.khawoat_rmbp.maidxcellent.Chat.util.BadgeUtils;
import com.example.khawoat_rmbp.maidxcellent.Chat.util.HttpRequest;
import com.example.khawoat_rmbp.maidxcellent.MainAfterLogin;
import com.example.khawoat_rmbp.maidxcellent.R;
import com.example.khawoat_rmbp.maidxcellent.SocialLogin.LoginActivity;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.Since;
import com.sinch.android.rtc.SinchError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class splashScreen extends BaseActivity implements SinchService.StartFailedListener {

    private static String TAG = "SplashScreen";

    public static final int REQUEST_PERMISSION_READ_PHONE_STATE= 2900;
    public static final int REQUEST_PERMISSION_CAMERA= 3000;
    public static final int REQUEST_PERMISSION_STORAGE= 3100;
    public static final int REQUEST_PERMISSION_STORAGE_READ= 3200;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private DatabaseReference mFirebaseDatabase;
    public final static int TERMINAL_ANIMATION = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNotificationBarTranparent();
        setContentView(R.layout.activity_splash_screen);

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("user");

        if (Config.FCM_TOKEN==null){
            Config.FCM_TOKEN = FirebaseInstanceId.getInstance().getToken();
        }
        sharedPreferences = getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        BadgeUtils.clearBadge(this);
        requestAppPermission();
    }

    private void requestAppPermission() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(splashScreen.this,
                        new String[]{android.Manifest.permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
            }
        } else {
            requestAppPermissionWriteStorage();
        }
    }

    private void requestAppPermissionWriteStorage() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(splashScreen.this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_STORAGE);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_STORAGE);
            }
        } else {
            requestAppPermissionReadStorage();
        }
    }

    private void requestAppPermissionReadStorage() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(splashScreen.this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_STORAGE_READ);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_STORAGE_READ);
            }
        } else {
            requestAppPermissionCall();
        }
    }

    private void requestAppPermissionCall() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_PHONE_STATE)) {
                ActivityCompat.requestPermissions(splashScreen.this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE}, REQUEST_PERMISSION_READ_PHONE_STATE);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE}, REQUEST_PERMISSION_READ_PHONE_STATE);
            }
        } else {
            getUUID();
        }
    }

    private void getUUID(){
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        Config.DEVICE_TOKEN = tManager.getDeviceId();
        delaySplashScreen();
    }

    private void delaySplashScreen() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isLogin()){
                    login();
                }else {
                    openActivityFinish(LoginActivity.class);
                }
            }
        }, 3000);
    }
    private boolean isLogin(){
        Config.USERNAME = sharedPreferences.getString("username", "");
        return sharedPreferences.getBoolean("isLogin", false);
    }
    private void login(){
        StringRequest request = new StringRequest(Request.Method.GET, HttpRequest.LOGIN, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                if(s.equals("null")){
                    Toast.makeText(splashScreen.this, "user not found", Toast.LENGTH_LONG).show();
                }
                else{
                    try {
                        JSONObject obj = new JSONObject(s);

                        if(!obj.has(sharedPreferences.getString("username", ""))){
                            openActivityFinish(LoginActivity.class);
                        }
                        else if(obj.getJSONObject(sharedPreferences.getString("username", "")).getString("password").equals(sharedPreferences.getString("password", ""))){
                            Config.username = sharedPreferences.getString("username", "");
                            Config.password = sharedPreferences.getString("password", "");
                            Config.ImageUser = sharedPreferences.getString("profile_pic", "");
                            editor = sharedPreferences.edit();
                            editor.putString(Config.LOGIN, obj.getJSONObject(sharedPreferences.getString("username", "")).toString());
                            editor.putBoolean("isLogin", true);
                            editor.putString("username", sharedPreferences.getString("username", ""));
                            editor.putString("password", sharedPreferences.getString("password", ""));
                            editor.putString("profile_pic", obj.getJSONObject(sharedPreferences.getString("username", "")).getString("image"));
                            editor.commit();
                            updateUser(Config.username);
                            //openActivityFinish(HomeActivity.class);
                        }
                        else {
                            openActivityFinish(LoginActivity.class);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(splashScreen.this);
        rQueue.add(request);
    }

    private void updateUser(String name) {
        HashMap<String,Object> taskMap = new HashMap<String,Object>();
        taskMap.put("tokan",  Config.FCM_TOKEN);
        mFirebaseDatabase.child(name).updateChildren(taskMap);
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(name);
        } else {
            openActivityFinish(LoginActivity.class);
        }
    }

    @Override
    protected void onResume() {
        SharedPreferences sharedPreferences = getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isLogout", false);
        editor.apply();
        super.onResume();
    }

    public static void show(Context context, int animation) {
        switch (animation) {
            case TERMINAL_ANIMATION:
                new TerminalAnimation(context).start();
                break;
            default:
                Log.e(TAG, "unknown animation");
        }
    }

    @Override
    public void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {

        openActivityFinish(MainAfterLogin.class);

    }


}

