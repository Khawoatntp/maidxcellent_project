package com.example.khawoat_rmbp.maidxcellent.SocialLogin;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.khawoat_rmbp.maidxcellent.BaseActivity;
import com.example.khawoat_rmbp.maidxcellent.Chat.services.SinchService;
import com.example.khawoat_rmbp.maidxcellent.Chat.util.HttpRequest;
import com.example.khawoat_rmbp.maidxcellent.MainAfterLogin;
import com.example.khawoat_rmbp.maidxcellent.R;
//import com.facebook.AppEventsLogger;
import com.example.khawoat_rmbp.maidxcellent.Register;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sinch.android.rtc.SinchError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends BaseActivity implements View.OnClickListener,SinchService.StartFailedListener{
    private CallbackManager callbackManager;
//    private LoginButton loginButton;
    private TextView btnLogin;
    private ProgressDialog progressDialog;
    User user;

    private TextView register;
    private EditText username, password;
    private TextView loginButton;
    private String userr , pass;

    private ProgressDialog pd;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog mSpinner;

    private DatabaseReference mFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //asking for permissions here
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.READ_PHONE_STATE},100);
        }

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("user");

        if (Config.FCM_TOKEN==null){
            Config.FCM_TOKEN = FirebaseInstanceId.getInstance().getToken();
        }
        sharedPreferences = getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);



        if(PreferenceUtils.getCurrentUser(LoginActivity.this) != null){
            LoginManager.getInstance().logOut();
            Intent homeIntent = new Intent(LoginActivity.this, MainAfterLogin.class);

            startActivity(homeIntent);

            finish();
        }
        bindView();
        setView();
    }

    private void bindView(){
        register = (TextView)findViewById(R.id.register);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        loginButton = (TextView)findViewById(R.id.loginButtonFire);
    }

    private void setView(){
        pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage("Loading...");

        register.setOnClickListener(this);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUser();
            }
        });
    }

    @Override
    public void onClick(View v) {

        if(v==register){
 //           openActivity(Register.class);
            startActivity(new Intent(LoginActivity.this, Register.class));
        }else if(v==loginButton){
            checkUser();
        }

    }

    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    @Override
    protected void onPause() {
        if (mSpinner != null) {
            mSpinner.dismiss();
        }
        super.onPause();
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        if (mSpinner != null) {
            mSpinner.dismiss();
        }
    }

    @Override
    public void onStarted() {

    }


    private void checkUser(){
        userr = username.getText().toString();
        pass = password.getText().toString();

        if(userr.equals("")){
            username.setError("can't be blank");
        }
        else if(pass.equals("")){
            password.setError("can't be blank");
        }
        else{
            Login();
        }
    }

    private void Login(){
        pd.show();
        StringRequest request = new StringRequest(Request.Method.GET, HttpRequest.LOGIN, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                if(s.equals("null")){
                    Toast.makeText(LoginActivity.this, "user not found", Toast.LENGTH_LONG).show();
                }
                else{
                    try {
                        JSONObject obj = new JSONObject(s);

                        if(!obj.has(userr)){
                            Toast.makeText(LoginActivity.this, "user not found", Toast.LENGTH_LONG).show();
                        }
                        else if(obj.getJSONObject(userr).getString("password").equals(pass)){
                            Config.username = userr;
                            Config.password = pass;
                            Config.ImageUser = obj.getJSONObject(userr).getString("image");
                            updateUser(userr);
                            editor = sharedPreferences.edit();
                            editor.putString(Config.LOGIN, obj.getJSONObject(userr).toString());
                            editor.putBoolean("isLogin", true);
                            editor.putString("username", userr);
                            editor.putString("password", pass);
                            editor.putString("profile_pic", obj.getJSONObject(userr).getString("image"));
                            editor.commit();
//                            if (!getSinchServiceInterface().isStarted()) {
//                                getSinchServiceInterface().startClient(userr);
//                            } else {
//                                Toast.makeText(LoginActivity.this, "Please enter a name", Toast.LENGTH_LONG).show();
//                            }
                            Log.d("login","login");
                            openActivityFinish(MainAfterLogin.class);
                        }
                        else {
                            Toast.makeText(LoginActivity.this, "incorrect password", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
                pd.dismiss();
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(LoginActivity.this);
        rQueue.add(request);
    }

    private void updateUser(String name) {
        HashMap<String,Object> taskMap = new HashMap<String,Object>();
        taskMap.put("tokan",  Config.FCM_TOKEN);
        mFirebaseDatabase.child(name).updateChildren(taskMap);
    }

    @Override
    protected void onResume() {
        super.onResume();


        callbackManager=CallbackManager.Factory.create();

        loginButton = (LoginButton)findViewById(R.id.login_button);

//        loginButton.setReadPermissions("public_profile", "email","user_friends");

        btnLogin= (TextView) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                loginButton.performClick();

                loginButton.setPressed(true);

                loginButton.invalidate();

//                loginButton.registerCallback(callbackManager, mCallBack);

                loginButton.setPressed(false);

                loginButton.invalidate();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

          //  progressDialog.dismiss();

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("response: ", response + "");
                            try {
                                String profile_url = "https://graph.facebook.com/" +  object.getString("id") + "/picture?type=large";
                                user = new User();
                                user.facebookID = object.getString("id").toString();
                                user.email = object.getString("email").toString();
                                user.name = object.getString("name").toString();
                                user.gender = object.getString("gender").toString();
                                user.image = profile_url;
                                PreferenceUtils.setCurrentUser(user,LoginActivity.this);

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            Toast.makeText(LoginActivity.this,"welcome "+user.name,Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(LoginActivity.this,MainAfterLogin.class);
                            startActivity(intent);
                            finish();

                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            progressDialog.dismiss();
        }
    };


}
